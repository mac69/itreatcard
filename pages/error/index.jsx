import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Box } from '@material-ui/core';


const Error = ()=> {
    return(
        <Box display="flex" height="100vh">
            <img alt="" src="/error.png" style={{width: "100%", height: "auto", objectFit: "scale-down"}}/>
        </Box>
    )
}

export default Error