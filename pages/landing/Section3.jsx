import React from 'react';
import { Container, Grid, Box } from '@material-ui/core';
import CardImg from '../../components/cardImg'

const Section3 = () => {
    return(
        <Container>
            <Box padding="5em 0">
                <Grid container spacing={4}>
                    <Grid item xs={12} md={6}>
                        <CardImg img="/sample1.png">
                            <span>Lorem ipsum dolor sit amet, consectetuer Lorem ipsum dolor sit amet, consectetuer Lorem ipsum dolor sit amet, consectetuer </span>
                        </CardImg>
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <CardImg img="/sample2.png">
                            <span>Lorem ipsum dolor sit amet, consectetuer Lorem ipsum dolor sit amet, consectetuer Lorem ipsum dolor sit amet, consectetuer </span>
                        </CardImg>
                    </Grid>
                </Grid>
            </Box>
        </Container>
       
    )
}

export default Section3