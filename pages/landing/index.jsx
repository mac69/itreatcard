import React from 'react';
import dynamic from "next/dynamic";
import Section1 from "./Section1"
import Section3 from "./Section3"

const Section2 = dynamic(() => import("./Section2"), {ssr: false});
const Section4 = dynamic(() => import("./Section4"), {ssr: false});
const Section5 = dynamic(() => import("./Section5"), {ssr: false});

const Landing = () => {
    return(
        <>
            <Section1/>
            <Section2/>
            <Section3/>
            <Section4/>
            {/*<Section5/> */}
        </>
    )
}

export default Landing