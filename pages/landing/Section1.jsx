import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Container, Grid, Box, Button } from '@material-ui/core';
import SubscribeForm from "../../components/SubscribeForm"

const useStyles = makeStyles(() => ({
    banner: {
        width: "100%", 
        margin: "3em 0"
    }
}));

const Banner = ()=> {
    const classes = useStyles();
    return(
        <Grid container direction="column">
            <Grid item>
                <img alt="" src="/banner.png" className={classes.banner}/>
            </Grid>
            <Grid item style={{textAlign: "center"}}>
                <span>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, <br/>
                    sed diam nonummy nibh euismod tincidunt ut laoreet dolore <br/>
                    magna aliquam erat volutpat. 
                </span>
            </Grid>
            <Grid item style={{textAlign: "center", marginTop: "2em"}}>
                <Button variant="contained" color="secondary">
                    VIEW MORE
                </Button>
            </Grid>
        </Grid>
    )
}

const Landing = () => {
    return(
        <Container>
            <Box paddingBottom="3em">
                <Grid container spacing={4}>
                    <Grid item xs={12} sm={12} md={9}>
                        <Banner/>
                    </Grid>
                    <Grid item xs={12} sm={12} md={3}>
                        <SubscribeForm/>
                    </Grid>
                </Grid>
            </Box>
        </Container>
    )
}

export default Landing