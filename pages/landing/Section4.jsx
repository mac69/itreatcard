import React from 'react';
import { Container, Grid, Box } from '@material-ui/core';
import Card2 from '../../components/card2' 

const data = [
    {
        title: "5-45% GIVER",
        items: ["0 commission", "Free listing on our app and website", "Free registration", "Free exposure", "Get more customer at your doorstep"],
        cta: "GET STARTED",
        highlight: false
    },
   {
        title: "THE BIG 50",
        items: ["Free Dashboard. This will help monitor your sales, give you real time reports and much more.", "Taking the lowest commision in the market", "No upfront costs"],
        cta: "GET STARTED",
        highlight: true
   }, 
   {
        title: "SELF CARE AND HEALTH",
        items: ["Free listing and marketing", "Customer service for your itreat clients", "Free Dashboard. Monitor your sales, give real time reports, etc", "Guaranteed customer and payment", "Higher return on customer", "No Upfront costs"],
        cta: "GET STARTED",
        highlight: false
   },
   {
       title: "AD PLACEMENT ",
       items: ["Free ad placement", "Free exposure", "Reports & Insight on campaigns"],
       cta: "CALL NOW!",
       highlight: false
   }
]

const Section4 = () => {
    return(
        <Container id="section4">
            <Box padding="5em 0">
                <Grid container spacing={4}>
                    {
                        data.map((data, index)=>
                            <Grid item xs={12} md={3} key={index}>
                                <Card2 title={data.title} items={data.items} cta={data.cta} highlight={data.highlight} isModulus={index % 2}/>
                            </Grid>
                        )}
                </Grid>
            </Box>
        </Container>
       
    )
}

export default Section4