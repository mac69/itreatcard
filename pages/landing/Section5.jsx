import React from 'react';
import { Container, Box } from '@material-ui/core';
import SignupForm from '../../components/signupForm'

const Section5 = () => {
    return(
        <Container>
            <Box padding="3em 0" display="flex" justifyContent="center">
                <Box maxWidth="900px" width="100%">
                    <SignupForm/>
                </Box>
            </Box>
        </Container>
       
    )
}

export default Section5