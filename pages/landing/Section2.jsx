import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Container, Box } from '@material-ui/core';
import Swiper from 'react-id-swiper';
import CardInfo from '../../components/cardInfo'

const useStyles = makeStyles(() => ({
    sliderCotnainer: {
        maxWidth: "800px",
        width: "100%"
    }
    
}));

const Landing = () => {
    const params = {
        effect: 'coverflow',
        grabCursor: true,
        centeredSlides: true,
        slidesPerView: 'auto',
        coverflowEffect: {
          rotate: 50,
          stretch: 0,
          depth: 100,
          modifier: 1,
          slideShadows: true
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        pagination: {
            el: '.swiper-pagination'
        }
    }

    const classes = useStyles();
    return (
        <Container>
            <Box padding="7em 0">
                <Swiper {...params}>
                    <div className={classes.sliderCotnainer}>
                        <CardInfo title="Selfcare and Health" img="/slider-item.png">
                            This type of sign up is for merchants whose business model is based on repeat visits, 
                            monthly / yearly sessions, classes, etc. Your customer goes on the app ->  
                            Clicks on your shop listing  ->Chooses the number of visits/sessions (minimum selection of 3) -> 
                            Pays in one go for all selected visits/sessions.
                            <br/><br/>
                            All you need to do is -> List our business ->  Provide a 10% discount for between 3-10 
                            secure visits, this will automatically be deducted on the app -> Give a 25 % commision 
                            which is the lowest in the market   
                        </CardInfo>
                    </div>
                    <div className={classes.sliderCotnainer}>
                        <CardInfo title="Selfcare and Health" img="/slider-item.png">
                            This type of sign up is for merchants whose business model is based on repeat visits, 
                            monthly / yearly sessions, classes, etc. Your customer goes on the app ->  
                            Clicks on your shop listing  ->Chooses the number of visits/sessions (minimum selection of 3) -> 
                            Pays in one go for all selected visits/sessions.
                            <br/><br/>
                            All you need to do is -> List our business ->  Provide a 10% discount for between 3-10 
                            secure visits, this will automatically be deducted on the app -> Give a 25 % commision 
                            which is the lowest in the market   
                        </CardInfo>
                    </div>
                    <div className={classes.sliderCotnainer}>
                        <CardInfo title="Selfcare and Health" img="/slider-item.png">
                            This type of sign up is for merchants whose business model is based on repeat visits, 
                            monthly / yearly sessions, classes, etc. Your customer goes on the app ->  
                            Clicks on your shop listing  ->Chooses the number of visits/sessions (minimum selection of 3) -> 
                            Pays in one go for all selected visits/sessions.
                            <br/><br/>
                            All you need to do is -> List our business ->  Provide a 10% discount for between 3-10 
                            secure visits, this will automatically be deducted on the app -> Give a 25 % commision 
                            which is the lowest in the market   
                        </CardInfo>
                    </div>
                </Swiper>
            </Box>
        </Container>
    )
}

export default Landing