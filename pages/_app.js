import React from 'react';
import App from 'next/app';
import Head from 'next/head';
import { ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import theme from '../src/theme';
import Bg from '../components/bg'
import Header from '../components/header'
import Footer from '../components/footer'
import 'swiper/css/swiper.css'

export default class MyApp extends App {
    componentDidMount() {
        // Remove the server-side injected CSS.
        // const jssStyles = document.querySelector('#jss-server-side');
        //     if (jssStyles) {
        //     jssStyles.parentElement.removeChild(jssStyles);
        // }
    }
  
    render() {
        const { Component, pageProps } = this.props;

        return (
            <>
                <Head>
                    <title>My page</title>
                    <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
                    <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet"></link>
                </Head>
                <ThemeProvider theme={theme}>
                    {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
                    <CssBaseline />
                    <Bg>
                        <Header/>
                        <Component {...pageProps} />
                        <Footer/>
                    </Bg>
                </ThemeProvider>
            </>
        );
    }
}