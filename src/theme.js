import { createMuiTheme } from '@material-ui/core/styles';

let theme = createMuiTheme({
	palette: {
		secondary: {
			main: '#FFC200',
		}
    },
    typography: {
		fontFamily: [
            'Poppins',
            'sans-serif'
		].join(',')
	}
});

theme = {
	...theme,
	overrides: {
		MuiButton: {
			root: {
				textTransform: "none",
			},
			outlinedSecondary: {
				color: theme.palette.primary,
				'&:hover': {
					background: theme.palette.secondary.main + "!important"
				}
			}
		}
	},
};

export default theme;
