import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Theme } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    footer: {
        backgroundImage: 'url(/footer.png)',
        backgroundRepeat: "no-repeat",
        height: "268px",
        position: "relative",
        [theme.breakpoints.up("xl")]: {
            backgroundSize: "100%"
        }
    },
    copyright: {
        textAlign: "center",
        position: "absolute",
        width: "100%",
        bottom: "29px",
    },
    text: {
        textDecoration: "none",
        color: theme.palette.text.primary,
        padding: "0 12px"
    }
}));

const Footer = () => {
    const classes = useStyles();
    return(
        <div className={classes.footer}>
            <div className={classes.copyright}>
                <span className={classes.text}>Copyright@2016 itreat.ae -FAQ</span>|
                <a className={classes.text} href="#">Privacy Policy</a>|
                <a className={classes.text} href="#">Terms & Condition</a>
            </div>
        </div>
    )
}

export default Footer