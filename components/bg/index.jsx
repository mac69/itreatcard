import React from 'react';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles(() => ({
    bg: {
        backgroundImage: 'url(/bg.png)',
        backgroundRepeat: "no-repeat",
        backgroundPositionX: "-90px",
        backgroundPositionY: "-34px"
    },
}));

const Bg =(props) => {
    const classes = useStyles();
    return(
       <div className={classes.bg}>
           {props.children}
       </div>
    )
}

export default Bg