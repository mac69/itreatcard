import React from 'react';
import { makeStyles, withStyles } from '@material-ui/styles';
import { Box, Grid, InputLabel, Button, Theme} from '@material-ui/core';
import Input from '../input'
import Hr from '../hr'

const useStyles = makeStyles((theme) => ({
    card: {
        padding: "2em 1.5em",
        borderRadius: "1.5em"
    },
    submitBtn: {
        borderRadius: "12px"
    },
    label: {
        fontSize: "26px",
        color: theme.palette.text.primary
    }
}));

const CustomInput = withStyles((theme )=> ({
    root: {
      'label + &': {
        // marginTop: theme.spacing(3),
      },
      width: "100%"
    },
    input: {
      borderRadius: "0",
      position: 'relative',
      backgroundColor: theme.palette.grey[100],
      border: '1px solid',
      borderColor: theme.palette.grey[100],
      fontSize: 16,
      width: '100%',
      padding: '10px 12px',
      transition: theme.transitions.create(['border-color', 'box-shadow']),
    },
}))(Input);

const SignupForm = () => {
    const classes = useStyles();
    return(
        <Box>
            <Grid container direction="column" spacing={2}>
                <Grid item>
                    <span style={{fontSize: "22px"}}>Lorem ipsum dolor sit amet, consectetuer adipiscing elit,</span><br/>
                    <span className={classes.label}>Sign Up</span>
                    <Hr/>
                </Grid>
                <Grid item>
                    <InputLabel className={classes.label}>
                        Company Name
                    </InputLabel>
                    <CustomInput/>
                </Grid>
                <Grid item>
                    <InputLabel className={classes.label}>
                        Mobile Number
                    </InputLabel>
                    <CustomInput/>
                </Grid>
                <Grid item>
                    <InputLabel className={classes.label}>
                        Industry
                    </InputLabel>
                    <CustomInput/>
                </Grid>
                <Grid item>
                    <InputLabel className={classes.label}>
                        Email
                    </InputLabel>
                    <CustomInput/>
                </Grid>
                <Grid item style={{textAlign: "center"}}>
                    <Button className={classes.submitBtn} size="small" variant="contained" color="secondary">
                        Submit now
                    </Button>
                </Grid>
            </Grid>
        </Box>
       
    )
}

export default SignupForm