import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Grid, Card, Button } from '@material-ui/core';
import Hr from '../hr'

const useStyles = makeStyles(() => ({
    card: {
        padding: "3em",
        backgroundColor: "#F6F6F6"
    },
    title: {
        fontSize: "26px"
    }
}));

const CardInfo = ({title, children, img}) => {
    const classes = useStyles();
    return(
        <Card className={classes.card}>
            <Grid container spacing={4}>
                <Grid item md={6}>
                    <Grid container>
                        <span className={classes.title}>{title}</span>
                        <Hr/>
                        {children}
                        <Button variant="contained" color="secondary" href="/#section4">Click here</Button>
                    </Grid>
                </Grid>
                <Grid item md={6}>
                    <img alt="" src={img}/>
                </Grid>
            </Grid>
        </Card>
    )
}

export default CardInfo