import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { CardMedia, CardContent, Card } from '@material-ui/core';
import dynamic from "next/dynamic";

// const Card = dynamic(import("@material-ui/core/card"));
const useStyles = makeStyles(() => ({
    card: {
        borderRadius: "2em"
    },
    media: {
        height: 300,
    },
}));

const CardImg = ({img, children})=> {
    const classes = useStyles();
    return(
        <Card className={classes.card}>
            <CardMedia className={classes.media} image={img}/>
            <CardContent>
                {children}
            </CardContent>
        </Card>
    )
}

export default CardImg