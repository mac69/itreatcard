import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Theme, Container } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    header: {
        // backgroundColor: theme.palette.secondary.main
    }
}));

const Header = () => {
    const classes = useStyles();
    return(
        <div className={classes.header}>
            <Container>
                <img alt="logo" src="/logo.png"/>
            </Container>
        </div>
    )
}

export default Header