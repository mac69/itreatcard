import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Theme, Card, Grid, Box, Button, useTheme } from '@material-ui/core';
import Hr from '../hr';


const useStyles = makeStyles((theme) => ({
   card: {
       padding: "2em",
       borderRadius: "2em",
       height: "100%"
   },
   button: {
       marginTop: "1em",
       borderRadius: "1.5em"
   }
}));

const CustomGrid = (props) =>  <Grid container direction="column" alignItems="center" style={{textAlign: "center"}}>{props.children}</Grid>

const Card2 = ({title, items, cta, highlight, isModulus}) => {
    const classes = useStyles();
    const theme = useTheme();

    return(
        // <Card className={classes.card} style={{backgroundColor: highlight ? theme.palette.secondary.main: "white", marginTop: isModulus? "2em" : 0}}></Card>
        <Card className={classes.card} style={{backgroundColor: highlight ? theme.palette.secondary.main: "white"}}>
            <Grid container style={{height: "100%"}}>
                <Box textAlign="center" width="100%">
                    <b>{title}</b>
                </Box>
                <Box width="100%">
                    <CustomGrid>
                        {items.map((data, index)=>
                            <Box width="100%" key={index}>
                                <span>{data}</span>
                                <Hr/>
                            </Box>
                        )}
                    </CustomGrid>
                </Box>
            </Grid>
            <Box textAlign="center" width="100%" marginTop="-50px" height="162px">
                <Button variant="outlined" color="secondary" className={classes.button}>
                    <b>{cta}</b>
                </Button>
            </Box>
        </Card>
    )
}

export default Card2