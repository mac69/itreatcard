import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Card, Grid, InputLabel, Button } from '@material-ui/core';
import dynamic from "next/dynamic";
import Input from '../input'

// const Button = dynamic(import("@material-ui/core/Button"))
const useStyles = makeStyles(() => ({
    card: {
        padding: "2em 1.5em",
        borderRadius: "1.5em",
        background: "transparent",
        boxShadow: "0px 2px 10px 10px rgba(0, 0, 0, 0.22), 0px 1px 1px 0px rgba(0, 0, 0, 0.3), 0px 1px 3px 0px rgba(0,0,0,0.12)"
    },
    submitBtn: {
        borderRadius: "12px"
    }
}));

const SubscribeForm = () => {
    const classes = useStyles();
    return(
        <Card className={classes.card}>
            <Grid container direction="column" spacing={2}>
                <Grid item>
                    <InputLabel>
                        Company Name
                    </InputLabel>
                    <Input/>
                </Grid>
                <Grid item>
                    <InputLabel>
                        Mobile Number
                    </InputLabel>
                    <Input/>
                </Grid>
                <Grid item>
                    <InputLabel>
                        Industry
                    </InputLabel>
                    <Input/>
                </Grid>
                <Grid item>
                    <InputLabel>
                        Email
                    </InputLabel>
                    <Input/>
                </Grid>
                <Grid item style={{textAlign: "center"}}>
                    <Button className={classes.submitBtn} size="small" variant="contained" color="secondary">
                        Submit now
                    </Button>
                </Grid>
            </Grid>
        </Card>
       
    )
}

export default SubscribeForm