import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Theme, Card, Grid, Box, Button } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
   card: {
       padding: "2em"
   },
   button: {
       marginTop: "1em",
       borderRadius: "1.5em"
   }
}));

const Card1 = ({title, description, isModulus}) => {
    const classes = useStyles();
    return(
        <Card className={classes.card} style={{marginTop: isModulus? "2em" : 0, marginLeft: isModulus ? 0 : "3em"}}>
            <Grid container>
                <Box textAlign="center" width="100%">
                    <b>{title}</b>
                </Box>
                <span>{description}</span>
                <Box textAlign="right" width="100%">
                    <Button variant="outlined" color="secondary" className={classes.button}>
                        <b>Click here</b>
                    </Button>
                </Box>
            </Grid>
        </Card>
    )
}

export default Card1